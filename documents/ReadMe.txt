softmax
-------------------------
static void softmax(float *input, int input_len)
{
   
    int i;
    float m;
    /* Find maximum value from input array */
    m = input[0];
    for (i = 1; i < input_len; i++) {
        if (input[i] > m) {
            m = input[i];
        }
    }

    float sum = 0;
    for (i = 0; i < input_len; i++) {
        sum += expf(input[i]-m);
    }

    for (i = 0; i < input_len; i++) {
        input[i] = expf(input[i] - m - log(sum));

    }    
}


tanh
--------------------------
tanh(x) = 2 * sigmoid(2x) - 1 


rule
----------------
max(0,x)
