function [ TrainArr , TestArr , ValidArr, TrainLabels, TestLabels, ValidLabels ] = LoadData( TrainArrF , TestArrF , ValidArrF, TrainLabelsF, TestLabelsF, ValidLabelsF )
%LOADDATA Summary of this function goes here
%   Detailed explanation goes here

load([TrainArrF '.mat']);
load([TestArrF '.mat']);
load([ValidArrF '.mat']);
load([TrainLabelsF '.mat']);
load([TestLabelsF '.mat']);
load([ValidLabelsF '.mat']);

%TrainArr = Normalize(TrainArr);
%TestArr = Normalize(TestArr);
%ValidArr = Normalize(ValidArr);


end

