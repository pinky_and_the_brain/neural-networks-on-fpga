%main function
function wineBP()
format long;
digits(15);

Xor = 1;
Mnst = 2;
Wines = 3;
dataType = Wines;
useBias = 1;

roundFactor =6;
if(dataType == Xor)
    [inputData, labels,  Weights] =  ReadXorData();
end
if (dataType == Mnst)
    [inputData, labels,  Weights, labelsOptionCount] = ReadMnistData();
end 
if (dataType == Wines)
    [inputData, labels,  Weights, labelsOptionCount] =  ReadData();
end

[~, Layers] = size(Weights);

[TestSize, ~ ] = size(inputData);
% add bias
if (useBias)
   inputData= [ones(TestSize,1) inputData];
end
%[~, Layers] = size(Weights);
LearningRate = 0.1;
if (dataType ~= Xor)
    targetValues = 0.*ones(size(labels, 1),labelsOptionCount);
    for n = 1: size(labels, 1)
        if (dataType == Mnst)
            targetValues(n, labels(n) + 1) = 1;
        end
        if (dataType == Wines)
            targetValues(n, labels(n) ) = 1;
        end
    end
end

% fprintf('***************************************************************************\n')
% fprintf('\t\t\t\t before all starts \n')
% fprintf('***************************************************************************\n')
% fprintf([repmat('%f\t', 1, size(Weights{1}, 2)) '\n'], Weights{1}')
% fprintf('\n')
% fprintf([repmat('%f\t', 1, size(Weights{2}, 2)) '\n'], Weights{2}')
% fprintf('\n')
% fprintf([repmat('%f\t', 1, size(Weights{3}, 2)) '\n'], Weights{3}')
% fprintf('\n')

%#EDI
%for r = 1 : size(Weights)
%    Weights{r} = round(Weights{r},roundFactor);
%end

for t=1:500	

    for i=1:178
%         selectedImageIndex = floor(rand(1)*TestSize + 1);
%        Nets{1} =   inputData(selectedImageIndex,:) * Weights{ 1}';
        data =  inputData(i,:);
        %fprintf([repmat('%.15f\t', 1, size(data, 2)) '\n'], data');
        Nets{1} =  data * Weights{ 1};
        %fprintf([repmat('%.15f\t', 1, size(Nets{1}, 2)) '\n'], Nets{1}');
        Outs{1} =  Activation(Nets{1});
        %fprintf([repmat('%.15f\t', 1, size(Outs{1}, 2)) '\n'], Outs{1}');
        %fprintf('\n');
        if (useBias)
            Outs{1}(: , 1) = 1;
        end
        for n = 2: Layers  
            %fprintf([repmat('%.15f\t', 1, size(Outs{n -1}, 2)) '\n'], Outs{n -1}');
            Nets{n} =   Outs{n -1} * Weights{n};
            %fprintf([repmat('%.15f\t', 1, size(Nets{n}, 2)) '\n'], Nets{n}');
            Outs{n} =   Activation(Nets{n} );       
            %fprintf([repmat('%.15f\t', 1, size(Outs{n}, 2)) '\n'], Outs{n}');
            %fprintf('\n');
             if(Layers > n)
                 if (useBias)
                   Outs{n}(:, 1) = 1;
                 end
             end
        end
        %fprintf('\n\n');
%         fprintf('***************************************************************************\n')
%         fprintf('\t\t\t\t after feeddorword example %d \n', i)
%         fprintf('***************************************************************************\n')
%         fprintf([repmat('%f\t', 1, size(Weights{1}, 2)) '\n'], Weights{1}')
%         fprintf('\n')
%         fprintf([repmat('%f\t', 1, size(Weights{2}, 2)) '\n'], Weights{2}')
%         fprintf('\n')
%          fprintf([repmat('%f\t', 1, size(Weights{3}, 2)) '\n'], Weights{3}')
%          fprintf('\n')
        if (dataType ~= Xor)            
            ErrorsOut{Layers} = targetValues(i,:) - Outs{Layers};
        else
            ErrorsOut{Layers} = labels(i) - Outs{Layers};
        end
        %fprintf([repmat('%.15f\t', 1, size(Outs{1}, 2)) '\n'], Outs{1}');
        %fprintf([repmat('%.15f\t', 1, size(Outs{2}, 2)) '\n'], Outs{2}');
        %fprintf([repmat('%.15f\t', 1, size(Outs{3}, 2)) '\n'], Outs{3}');
        for n = Layers - 1 : -1 : 1  
            %fprintf([repmat('%.15f\t', 1, size(dActivation(Nets{n}), 2)) '\n'], dActivation(Nets{n})');
            
            ErrorsOut{n} = ((Weights{n + 1} * ErrorsOut{n + 1}' ) .* (Outs{n} .* dActivation(Nets{n}) )')';
            %fprintf([repmat('%.15f\t', 1, size(ErrorsOut{n}, 2)) '\n'], ErrorsOut{n}');
            %fprintf([repmat('%.15f\t', 1, size(Outs{n}, 2)) '\n'], Outs{n}');
        end
        %fprintf('\n');
        
        for n = Layers: -1 : 2            
            Weights{n} =  Weights{n} + LearningRate * Outs{n - 1}' * ErrorsOut{n};
        end
        
        Weights{1} =  Weights{1} + LearningRate *  data' * ErrorsOut{1};     
%         fprintf('***************************************************************************\n')
%         fprintf('\t\t\t\t after backpropagation example %d \n', i)
%         fprintf('***************************************************************************\n')
%         fprintf([repmat('%f\t', 1, size(Weights{1}, 2)) '\n'], Weights{1}')
%         fprintf('\n')
%         fprintf([repmat('%f\t', 1, size(Weights{2}, 2)) '\n'], Weights{2}')
%         fprintf('\n')
%          fprintf([repmat('%f\t', 1, size(Weights{3}, 2)) '\n'], Weights{3}')
%          fprintf('\n')
    end
%  fprintf([repmat('%f\t', 1, size(Weights{4}, 2)) '\n'], Weights{4}')
%          fprintf('\n')
% 
%         fileID = fopen('C:\Users\Adi\Downloads\InputfromEdi\result_Epoc_3_1_perEpoc_withBais.txt','w');
%         fprintf(fileID,'*********************************************************************************************************************************************\n')
%         fprintf(fileID,'epoc %d \n', t);
%         fprintf(fileID,'Weights1\n');
%         fprintf(fileID,[repmat('%f\t', 1, size(Weights{1}, 2)) '\n'], Weights{1}');
%         fprintf(fileID,'\n');
%         fprintf(fileID,'Weights2\n');
%         fprintf(fileID,[repmat('%f\t', 1, size(Weights{2}, 2)) '\n'], Weights{2}');
%         fprintf(fileID,'\n');
%         fprintf(fileID,'Weights3\n');
%         fprintf(fileID,[repmat('%f\t', 1, size(Weights{3}, 2)) '\n'], Weights{3}');
%         fprintf(fileID,'\n');
%         fclose(fileID);

%         fprintf('*********************************************************************************************************************************************\n')
%         fprintf('epoc %d \n', t);
%         fprintf('Weights1\n');
%         fprintf([repmat('%f\t', 1, size(Weights{1}, 2)) '\n'], Weights{1}');
%         fprintf('\n');
%         fprintf('Weights2\n');
%         fprintf([repmat('%f\t', 1, size(Weights{2}, 2)) '\n'], Weights{2}');
%         fprintf('\n');
         
         %fprintf('Weights3\n');
         %fprintf([repmat('%.15f\t', 1, size(Weights{3}, 2)) '\n'], Weights{3}');
         %fprintf('\n');

    error = 0;

    %for i = 1:TestSize
    for i = 1:178
        data =  inputData(i,:);
        Nets{1} =    data * Weights{ 1};
        Outs{1} =   Activation(Nets{1} );
        Outs{1}(: , 1) = 1;
        for n = 2: Layers
            Nets{n} =   Outs{n -1} * Weights{n};
            Outs{n} =  Activation(Nets{n}  );    
             if(Layers > n)
                   Outs{n}(:, 1) = 1;  
             end
             
             
        end
         [~,idx] = max(Outs{n});
         if (dataType == Mnst)
             e = ((idx-1) ~= labels(i));
             if(e)
              error = error + ((idx-1) ~= labels(i));
             end
         end
              
         if (dataType == Wines)
            error = error + (idx ~= labels(i));
         end
    end
    
    error = error/178;
    
    fprintf('outs:');
    fprintf([repmat('%.15f\t', 1, size(Outs{n}, 2)) '\n'], Outs{n})
    fprintf('epocs: %d Error: %f \n', t, error)
end

end

function [inputData, labels, Weights, labelsOptionCount] = ReadData()
filename = 'weight1.data';
delimiterIn = ',';
Weights{1} = importdata(filename,delimiterIn);
filename = 'weight2.data';
Weights{2} = importdata(filename,delimiterIn);
filename = 'weight3.data';
Weights{3} = importdata(filename,delimiterIn);
filename = 'wine.data';
inputData  = importdata(filename,delimiterIn);
labels = inputData(:,1 );
inputData = inputData(:,2:end);
labelsOptionCount = 3;
end


function [inputData, labels, Weights, labelsOptionCount] = ReadMnistData()
filename = 'weight1Mnist.data';
delimiterIn = ',';
Weights{1} = importdata(filename,delimiterIn);
Weights{1} = Weights{1};
filename = 'weight2Mnist.data';
Weights{2} = importdata(filename,delimiterIn);
Weights{2} = Weights{2} ;
filename = 'weight4Mnist.data';
Weights{3} = importdata(filename,delimiterIn);
Weights{3} = Weights{3};
 [inputData, labels, ~ ] = readMNISTTrainSet();
 labelsOptionCount = 10;
end


function [inputData, labels, Weights] = ReadXorData()
filename = 'weight1XOR.data';
delimiterIn = ',';
Weights{1} = importdata(filename,delimiterIn);
filename = 'weight2XOR.data';
Weights{2} = importdata(filename,delimiterIn);
filename = 'weight3XOR.data';
Weights{3} = importdata(filename,delimiterIn);
filename = 'XOR.data';
inputData  = importdata(filename,delimiterIn);
labels = inputData(:,1 );
inputData = inputData(:,2:end);
end


function [imgs labels nImages] = readMNISTTrainSet()
    
fp = fopen('train-images.idx3-ubyte', 'rb');


fread(fp, 1, 'int32', 0, 'ieee-be');

nImages = fread(fp, 1, 'int32', 0, 'ieee-be');
nRows = fread(fp, 1, 'int32', 0, 'ieee-be');
nCols = fread(fp, 1, 'int32', 0, 'ieee-be');

imgs = fread(fp, inf, 'unsigned char');
imgs = reshape(imgs, nCols, nRows, nImages);
imgs = permute(imgs,[2 1 3]);

fclose(fp);

% Reshape to #pixels x #examples
imgs = reshape(imgs, size(imgs, 1) * size(imgs, 2), size(imgs, 3));
% Convert to double and rescale to [0,1]
imgs = double(imgs) / max(imgs(:));
imgs = imgs';

fp = fopen('train-labels.idx1-ubyte', 'rb');


fread(fp, 1, 'int32', 0, 'ieee-be');


nLabels = fread(fp, 1, 'int32', 0, 'ieee-be');

labels = fread(fp, inf, 'unsigned char');

assert(size(labels,1) == nLabels, 'Mismatch in label count');

fclose(fp);
end


function y=Activation(z)
  y =  1./(1+exp(-z));
end

function y=dActivation(z) 
  t = Activation(z);
  y= t.*(1-t);
end


function T=GetSigmoidActivationLookUp()
 dx = 0.01;
 x = -20:dx:20;
 N = 1000;
 x = linspace(-20,20,N);
 y =  1./(1+exp(-x));
 T = [x(:),y(:)];
end