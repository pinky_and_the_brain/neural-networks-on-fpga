#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "ocl.h"
#include "neural_network_layer.h"
#include "data_reader.h"
#include "neural_network.h"

//const int BATCH_SIZE = 30000;
const int EPOCS = 5;
/*
 * main execution routine
 */
int _tmain(int argc, TCHAR* argv[])
{
	GeneralDataSet dataSet = GetMnstDataSet();
	//GeneralDataSet dataSet = GetXORDataSet();
	//GeneralDataSet dataSet = GetWinesDataSet();
	if (CL_SUCCESS != ExecuteNeuralNetwork(dataSet, EPOCS))
	{
		return -1;
	}
    return 0;
}