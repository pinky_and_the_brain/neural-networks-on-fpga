#pragma once

#include "data_reader.h"

struct nn_args
{
	nn_args();
	~nn_args();

	cl_mem storage;
	cl_mem settings;
	cl_mem images;
	cl_mem labels;
	cl_mem batchSize;	
};

/*
* Generate random value for input buffers
*/
void generateInput(cl_double* inputArray, cl_uint arrayWidth, cl_uint arrayHeight);

/*
* Create OpenCL buffers from host memory
* These buffers will be used later by the OpenCL kernel
*/
int CreateBufferArguments(ocl_args_d_t *ocl, nn_args *nn, cl_double* storage, cl_uint* settings,
	cl_double* images, cl_double* labels, cl_uint batchSize, cl_uint storageSize,
	cl_uint settingsSize, cl_uint imagesSize, cl_uint labelsSize);

/*
* Set kernel arguments
*/
cl_uint SetKernelArguments(ocl_args_d_t *ocl, nn_args *nn);

void PrintMatrix(char* title, cl_double* mat, cl_uint columns, cl_uint rows);

cl_uint ExecuteNeuralNetwork(GeneralDataSet dataSet, cl_uint epocs);
/*
* "Read" the result buffer (mapping the buffer to the host memory address)
*/
void ReadOutput(ocl_args_d_t *ocl, nn_args *nn, cl_uint storageSize, cl_double *storage);


void CopyDataToStorage(cl_double* source, cl_double* destination, cl_int size);

void CopyDataToStorage(cl_int* source, cl_double* destination, cl_int size);

struct BatchDataSet
{
	int recordSize;
	int labelSize;
	int recordsCount;
	double* records;
	double* labels;
};

BatchDataSet GetBatchOfRecordsAndLabels(int amountOfExamples, GeneralDataSet dataSet, bool isTraining, bool isRandom);