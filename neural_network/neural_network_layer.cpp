#include <time.h>
#include "ocl.h"
#include "neural_network.h"
#include "neural_network_layer.h"


nn_args::nn_args() :
	storage(NULL),
	settings(NULL),
	images(NULL),
	labels(NULL),
	batchSize(NULL)
{
}

/*
* destructor - called only once
* Release all OpenCL objects
* This is a regular sequence of calls to deallocate all created OpenCL resources in bootstrapOpenCL.
*
* You may want to call these deallocation procedures in the middle of your application execution
* (not at the end) if you don't further need OpenCL runtime.
* You may want to do that in order to free some memory, for example,
* or recreate OpenCL objects with different parameters.
*
*/
nn_args::~nn_args()
{
	cl_int err = CL_SUCCESS;

	if (storage)
	{
		err = clReleaseMemObject(storage);
		if (CL_SUCCESS != err)
		{
			LogError("Error: clReleaseMemObject returned '%s'.\n", TranslateOpenCLError(err));
		}
	}
	if (settings)
	{
		err = clReleaseMemObject(settings);
		if (CL_SUCCESS != err)
		{
			LogError("Error: clReleaseMemObject returned '%s'.\n", TranslateOpenCLError(err));
		}
	}
	if (images)
	{
		err = clReleaseMemObject(images);
		if (CL_SUCCESS != err)
		{
			LogError("Error: clReleaseMemObject returned '%s'.\n", TranslateOpenCLError(err));
		}
	}
	if (labels)
	{
		err = clReleaseMemObject(labels);
		if (CL_SUCCESS != err)
		{
			LogError("Error: clReleaseMemObject returned '%s'.\n", TranslateOpenCLError(err));
		}
	}
	if (batchSize)
	{
		err = clReleaseMemObject(batchSize);
		if (CL_SUCCESS != err)
		{
			LogError("Error: clReleaseMemObject returned '%s'.\n", TranslateOpenCLError(err));
		}
	}
	
	/*
	* Note there is no procedure to deallocate platform
	* because it was not created at the startup,
	* but just queried from OpenCL runtime.
	*/
}


void CopyDataToStorage(cl_int* source, cl_double* destination, cl_int size)
{
	for (cl_int i = 0; i < size; ++i)
	{
		destination[i] = source[i] * 1.0f;
	}
}


void CopyDataToStorage(cl_double* source, cl_double* destination, cl_int size)
{
	for (cl_int i = 0; i < size; ++i)
	{
		destination[i] = source[i];
	}
}
/*
* Generate random value for input buffers
*/
void generateInput(cl_double* inputArray, cl_uint arrayWidth, cl_uint arrayHeight)
{
	// random initialization of input
	cl_uint array_size = arrayWidth * arrayHeight;
	for (cl_uint i = 0; i < array_size; ++i)
	{
		inputArray[i] = 1.0f / (double)(unsigned)GetRandIntFromRange(100, 10000);
	}
}

/*
* Create OpenCL buffers from host memory
* These buffers will be used later by the OpenCL kernel
*/
int CreateBufferArguments(ocl_args_d_t *ocl, nn_args *nn, cl_double* storage, cl_uint* settings,
	cl_double* images, cl_double* labels, cl_uint batchSize, cl_uint storageSize, cl_uint settingsSize, cl_uint imagesSize, cl_uint labelsSize)
{
	cl_int err = CL_SUCCESS;

	// Create new OpenCL buffer objects
	// As these buffer are used only for read by the kernel, you are recommended to create it with flag CL_MEM_READ_ONLY.
	// Always set minimal read/write flags for buffers, it may lead to better performance because it allows runtime
	// to better organize data copying.
	// You use CL_MEM_COPY_HOST_PTR here, because the buffers should be populated with bytes at inputA and inputB.

	nn->storage = clCreateBuffer(ocl->context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_double) * storageSize, storage, &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateBuffer for storage returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	nn->settings = clCreateBuffer(ocl->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_uint) * settingsSize, settings, &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateBuffer for settings returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	nn->images = clCreateBuffer(ocl->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_double) * (imagesSize * batchSize), images, &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateBuffer for expectedResult returned %s\n", TranslateOpenCLError(err));
		return err;
	}
	
	nn->labels = clCreateBuffer(ocl->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_double) * (labelsSize * batchSize), labels, &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateBuffer for expectedResult returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	nn->batchSize = clCreateBuffer(ocl->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &batchSize, &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateBuffer for expectedResult returned %s\n", TranslateOpenCLError(err));
		return err;
	}


	return CL_SUCCESS;
}

/*
* Set kernel arguments
*/
cl_uint SetKernelArguments(ocl_args_d_t *ocl, nn_args *nn)
{
	cl_int err = CL_SUCCESS;

	err = clSetKernelArg(ocl->kernel, 0, sizeof(cl_mem), (void *)&nn->storage);
	if (CL_SUCCESS != err)
	{
		LogError("Error: Failed to set argument storage, returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	err = clSetKernelArg(ocl->kernel, 1, sizeof(cl_mem), (void *)&nn->settings);
	if (CL_SUCCESS != err)
	{
		LogError("error: Failed to set argument settings, returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	err = clSetKernelArg(ocl->kernel, 2, sizeof(cl_mem), (void *)&nn->images);
	if (CL_SUCCESS != err)
	{
		LogError("error: Failed to set argument images, returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	err = clSetKernelArg(ocl->kernel, 3, sizeof(cl_mem), (void *)&nn->labels);
	if (CL_SUCCESS != err)
	{
		LogError("error: Failed to set argument labels, returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	err = clSetKernelArg(ocl->kernel, 4, sizeof(cl_mem), (void *)&nn->batchSize);
	if (CL_SUCCESS != err)
	{
		LogError("error: Failed to set argument batchSize, returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	return err;
}

/*
* "Read" the result buffer (mapping the buffer to the host memory address)
*/
void ReadOutput(ocl_args_d_t *ocl, nn_args *nn, cl_uint storageSize, cl_double *storage)
{
	cl_int err = CL_SUCCESS;

	// Enqueue a command to map the buffer object (ocl->dstMem) into the host address space and returns a pointer to it
	// The map operation is blocking
	cl_double *resultPtr = (cl_double *)clEnqueueMapBuffer(ocl->commandQueue, nn->storage, true, CL_MAP_READ, 0, sizeof(cl_double) * storageSize, 0, NULL, NULL, &err);

	if (CL_SUCCESS != err)
	{
		LogError("Error: clEnqueueMapBuffer returned %s\n", TranslateOpenCLError(err));
	}

	// Call clFinish to guarantee that output region is updated
	err = clFinish(ocl->commandQueue);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clFinish returned %s\n", TranslateOpenCLError(err));
	}

	for (cl_uint i = 0; i < storageSize; ++i)
	{
		storage[i] = resultPtr[i];
	}

	// Unmapped the output buffer before releasing it
	err = clEnqueueUnmapMemObject(ocl->commandQueue, nn->storage, resultPtr, 0, NULL, NULL);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clEnqueueUnmapMemObject returned %s\n", TranslateOpenCLError(err));
	}

	//_aligned_free(resultPtr);
}

/// <summary>
/// Create a general batch dataset with records and corrisponding labels 
/// taken from predefined records container.
/// </summary>
/// <param name="amountOfExamples">The number of records in the batch</param>
/// <param name="dataSet">The original dataset (records pool)</param>
/// <param name="isTraining">Define if the records are taken from the traingng records</param>
/// <param name="isRandom">Define if the records are being pulled randomly</param>
/// <returns>Records batch dataset</returns>
BatchDataSet GetBatchOfRecordsAndLabels(int amountOfExamples, GeneralDataSet dataSet, bool isTraining, bool isRandom)
{
	BatchDataSet resultBatchData;
	resultBatchData.records = new double[amountOfExamples * dataSet.recordSize];
	resultBatchData.labels = new double[amountOfExamples * dataSet.labelSize];
	resultBatchData.recordSize = dataSet.recordSize;
	resultBatchData.labelSize = dataSet.labelSize;
	resultBatchData.recordsCount = amountOfExamples;
	  
	for (int i = 0; i < amountOfExamples; i++) {
		int rndI = 0;
		if (isTraining)
		{
			rndI = i % dataSet.trainRecordsCount;
		}
		else
		{
			rndI = i % dataSet.recordsCount;
		}

		if (isRandom)
		{
			if (isTraining)
			{
				rndI = GetRandIntFromRange(0, dataSet.trainRecordsCount - 1);
			}
			else
			{
				rndI = GetRandIntFromRange(0, dataSet.recordsCount - 1);
			}
		}

		for (int j = 0; j < dataSet.recordSize; ++j)
		{
			if (isTraining)
			{
				resultBatchData.records[(i * resultBatchData.recordSize) + j] = (double)dataSet.trainRecords[rndI][j];
			}
			else
			{
				resultBatchData.records[(i * resultBatchData.recordSize) + j] = (double)dataSet.records[rndI][j];
			}
		}

		for (int j = 0; j < dataSet.labelSize; ++j)
		{
			if (isTraining)
			{
				resultBatchData.labels[i * resultBatchData.labelSize + j] = dataSet.trainLabels[rndI][j];
			}
			else
			{
				resultBatchData.labels[i * resultBatchData.labelSize + j] = dataSet.labels[rndI][j];
			}
		}
	}

	return resultBatchData;
}

void PrintMatrix(char* title, cl_double* mat, cl_uint columns, cl_uint rows)
{
	printf("%s:\n", title);
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < columns; ++j)
		{
			printf("[%.15f]", mat[i*columns + j]);
		}
		printf("\n");
	}
}

void PrintMatrix(char* title, double** mat, int columns, int rows)
{
	printf("%s:\n", title);
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < columns; ++j)
		{
			printf("[%.15f]", mat[i][j]);
		}
		printf("\n");
	}
}

cl_uint ExecuteNeuralNetwork(GeneralDataSet dataSet, cl_uint epocs)
{
	cl_int err;
	ocl_args_d_t ocl;
	cl_device_type deviceType = CL_DEVICE_TYPE_GPU;

	//initialize Open CL objects (context, queue, etc.)
	err = SetupOpenCL(&ocl, deviceType);
	if (CL_SUCCESS != err)
	{
		return err;
	}

	cl_uint layer1InputSize = dataSet.recordSize;
	cl_uint layer1NeuronsCount = dataSet.level1MatrixWidth;
	cl_uint layer1OutputSize = dataSet.level1MatrixWidth;
	cl_uint layer2InputSize = dataSet.level1MatrixWidth;
	cl_uint layer2NeuronsCount = dataSet.level2MatrixWidth;
	cl_uint layer2OutputSize = dataSet.level2MatrixWidth;
	cl_uint layer3InputSize = dataSet.level2MatrixWidth;
	cl_uint layer3NeuronsCount = dataSet.labelSize;
	cl_uint layer3OutputSize = dataSet.labelSize;

	cl_uint layers = 3;

	// first layer
	cl_double* layer1Input;
	cl_double* layer1Wheights;
	cl_double* layer1Output;
	cl_double* layer1Errors;
	cl_double* layer1DeactivatedOutput;

	// allocate working buffers. 
	// the buffer should be aligned with 4K page and size should fit 64-byte cached line
	cl_uint layer1WheightsSize = layer1NeuronsCount * layer1InputSize;
	cl_uint layer1ErrorsSize = layer1InputSize;
	cl_uint layer1DeactivatedOutputSize = layer1OutputSize;

	// second layer
	cl_double* layer2Input;
	cl_double* layer2Wheights;
	cl_double* layer2Output;
	cl_double* layer2Errors;
	cl_double* layer2DeactivatedOutput;

	// allocate working buffers. 
	// the buffer should be aligned with 4K page and size should fit 64-byte cached line
	cl_uint layer2WheightsSize = layer2NeuronsCount * layer2InputSize;
	cl_uint layer2ErrorsSize = layer2InputSize;
	cl_uint layer2DeactivatedOutputSize = layer2OutputSize;

	// third layer
	cl_double* layer3Input;
	cl_double* layer3Wheights;
	cl_double* layer3Output;
	cl_double* layer3Errors;
	cl_double* layer3DeactivatedOutput;
	cl_double* layer3ActivatedOutput;
	cl_double* layer3OutputErrors;

	// allocate working buffers. 
	// the buffer should be aligned with 4K page and size should fit 64-byte cached line
	cl_uint layer3WheightsSize = layer3NeuronsCount * layer3InputSize;
	cl_uint layer3ErrorsSize = layer3InputSize;
	cl_uint layer3ActivatedOutputSize = layer3OutputSize;
	cl_uint layer3DeactivatedOutputSize = layer3OutputSize;
	cl_uint layer3OutputErrorsSize = layer3OutputSize;

	cl_uint storageSize = layer1InputSize + layer1WheightsSize + layer1OutputSize + layer1ErrorsSize + layer1DeactivatedOutputSize +
		layer2InputSize + layer2WheightsSize + layer2OutputSize + layer2ErrorsSize + layer2DeactivatedOutputSize + 
		layer3InputSize + layer3WheightsSize + layer3OutputSize + layer3ErrorsSize + layer3DeactivatedOutputSize + layer3ActivatedOutputSize + layer3OutputErrorsSize;
	cl_uint storageSizeOptimized = ((sizeof(cl_double) * storageSize - 1) / 64 + 1) * 64;
	cl_double* storage = (cl_double*)_aligned_malloc(storageSizeOptimized, 4096);

	// allocate working buffers. 
	// the buffer should be aligned with 4K page and size should fit 64-byte cached line
	cl_uint settingsSize = (4 + (2 * layers));
	cl_uint settingsSizeOptimized = ((sizeof(cl_uint) * settingsSize - 1) / 64 + 1) * 64;
	cl_uint* settings = (cl_uint*)_aligned_malloc(settingsSizeOptimized, 4096);

	cl_uint expectedResultSizeOptimized = ((sizeof(cl_double) * layer3OutputSize - 1) / 64 + 1) * 64;
	cl_double* expectedResult = (cl_double*)_aligned_malloc(expectedResultSizeOptimized, 4096);

	if (NULL == storage || NULL == settings || NULL == expectedResult)
	{
		LogError("Error: _aligned_malloc failed to allocate buffers.\n");
		return err;
	}

	// setup settings
	settings[0] = 0; //use bias
	settings[1] = 1; //is learning
	settings[2] = 0; //is none parallel
	settings[3] = layers;
	// layer1
	settings[4] = layer1InputSize; // input size
	settings[5] = layer1NeuronsCount; // neurons count
	// layer2
	settings[6] = layer2InputSize; // input size
	settings[7] = layer2NeuronsCount; // neurons count
	// layer3
	settings[8] = layer3InputSize; // input size
	settings[9] = layer3NeuronsCount; // neurons count

	if (dataSet.useBias)
	{
		settings[0] = 1; //use bias
	}

	printf("layerSettings: %d %d %d %d %d %d", settings[3], settings[4], settings[5], settings[6], settings[7], settings[8]);
	// setup storage
	layer1Input = storage;
	layer1Wheights = layer1Input + layer1InputSize;
	layer1Output = layer1Wheights + layer1WheightsSize;
	layer1Errors = layer1Output + layer1OutputSize;
	layer1DeactivatedOutput = layer1Errors + layer1ErrorsSize;

	layer2Input = layer1DeactivatedOutput + layer1DeactivatedOutputSize;
	layer2Wheights = layer2Input + layer2InputSize;
	layer2Output = layer2Wheights + layer2WheightsSize;
	layer2Errors = layer2Output + layer2OutputSize;
	layer2DeactivatedOutput = layer2Errors + layer2ErrorsSize;

	layer3Input = layer2DeactivatedOutput + layer2DeactivatedOutputSize;
	layer3Wheights = layer3Input + layer3InputSize;
	layer3Output = layer3Wheights + layer3WheightsSize;
	layer3Errors = layer3Output + layer3OutputSize;
	layer3DeactivatedOutput = layer3Errors + layer3ErrorsSize;
	layer3ActivatedOutput = layer3DeactivatedOutput + layer3DeactivatedOutputSize;
	layer3OutputErrors = layer3ActivatedOutput + layer3ActivatedOutputSize;

	for (int i = 0; i < layer1InputSize; ++i)
	{
		for (int j = 0; j < layer1NeuronsCount; ++j)
		{
			layer1Wheights[(i * layer1NeuronsCount) + j] = dataSet.level1Matrix[i][j];
		}
	}

	for (int i = 0; i < layer2InputSize; ++i)
	{
		for (int j = 0; j < layer2NeuronsCount; ++j)
		{
			layer2Wheights[(i * layer2NeuronsCount) + j] = dataSet.level2Matrix[i][j];
		}
	}

	for (int i = 0; i < layer3InputSize; ++i)
	{
		for (int j = 0; j < layer3NeuronsCount; ++j)
		{
			layer3Wheights[(i * layer3NeuronsCount) + j] = dataSet.level3Matrix[i][j];
		}
	}

	// Create and build the OpenCL program
	err = CreateAndBuildProgram(&ocl, "neural_network_layer.cl");
	if (CL_SUCCESS != err)
	{
		return err;
	}

	// Program consists of kernels.
	// Each kernel can be called (enqueued) from the host part of OpenCL application.
	// To call the kernel, you need to create it from existing program.
	ocl.kernel = clCreateKernel(ocl.program, "NeuralNetworkExecuter", &err);
	if (CL_SUCCESS != err)
	{
		LogError("Error: clCreateKernel returned %s\n", TranslateOpenCLError(err));
		return err;
	}

	//printf("Before running example\n");
	//PrintMatrix("Weights 1", layer1Wheights, layer1OutputSize, layer1InputSize);
	//PrintMatrix("Weights 2", layer2Wheights, layer2OutputSize, layer2InputSize);
	//PrintMatrix("Init Weights 3", layer3Wheights, layer3OutputSize, layer3InputSize);
	// learning
	BatchDataSet batch;
	
	for (cl_uint i = 0; i < epocs * 2; ++i)
	{
		nn_args nn;
		cl_uint recordsCount = dataSet.trainRecordsCount;
		settings[1] = 1;// learning
		//every odd epoc is a testing
		if ((i % 2) == 1)
		{
			printf("\nepoc %d :", ((i/2)+1));
			settings[1] = 0;// testing
			recordsCount = dataSet.recordsCount;// recordsCount;
		}

		batch = GetBatchOfRecordsAndLabels(recordsCount, dataSet, (settings[1] == 1), true);
		err = CreateBufferArguments(&ocl, &nn, storage, settings, batch.records, batch.labels, recordsCount,
			storageSize, // storage size 
			settingsSize, // settings size 
			batch.recordSize, batch.labelSize);

		if (CL_SUCCESS != err)
		{
			return err;
		}

		// Passing arguments into OpenCL kernel.
		err = SetKernelArguments(&ocl, &nn);
		if (CL_SUCCESS != err)
		{
			return err;
		}

		// Execute (enqueue) the kernel
		cl_uint workgroups = 1;
		cl_uint workitems = 1;
		if (settings[2] == 0)
		{
			workitems = GetWorkItems(max(dataSet.recordSize, layer1NeuronsCount));
		}

		clock_t begin = clock();
		if (CL_SUCCESS != ExecuteKernel(&ocl, workitems, workgroups))
		{
			return err;
		}
		clock_t end = clock();

		ReadOutput(&ocl, &nn, storageSize, storage);

		if (i == 0)
		{
			//PrintMatrix("Weights 1", layer1Wheights, layer1OutputSize, layer1InputSize);
			//PrintMatrix("Weights 2", layer2Wheights, layer2OutputSize, layer2InputSize);
			//PrintMatrix("Weights 3", layer3Wheights, layer3OutputSize, layer3InputSize);
		}

		free(batch.records);
		free(batch.labels);
	}

	_aligned_free(storage);
	_aligned_free(settings);
	_aligned_free(expectedResult);

	int s;
	scanf("done! %d", &s);

	return CL_SUCCESS;
}
