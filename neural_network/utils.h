#include <d3d9.h>

#pragma once

// Print useful information to the default output. 
// Same usage as with printf.
void LogInfo(const char* str, ...);

// Print error notification to the default output. 
// Same usage as with printf.
void LogError(const char* str, ...);

// Generate an integer random value ranged between from and to values.
int GetRandIntFromRange(int from, int to);