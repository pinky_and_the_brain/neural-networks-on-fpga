#if defined(cl_khr_fp64)  // Khronos extension available?
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#define DOUBLE_SUPPORT_AVAILABLE
#elif defined(cl_amd_fp64)  // AMD extension available?
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#define DOUBLE_SUPPORT_AVAILABLE
#endif

#define SIGMOID(x) ((double)1.0 / (1 + exp(-x))) // only -100 < x < 100 is a valid value for the exp function 
#define UNSIGMOID(x) ((double)SIGMOID(x) * (1-SIGMOID(x))) //((double)sigX * (1-sigX))
#define LEARNING_RATE (double)0.1

// Helper function for printing matrix data to the consule
__kernel void PrintMatrix(__constant char* title, __global double* mat, uint columns, uint rows)
{
	// Row index
	uint id = get_global_id(0) + (get_global_size(0) * get_global_id(1));
	if (id == 0)
	{
		printf("%s:\n", title);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < columns; ++j)
			{
				printf("[%.15f]", mat[i*columns + j]);
			}
			printf("\n");
		}
	}
}

__kernel void ActivateSigmoidNoneParallel(
	__global double* input, __global double* output, uint inputSize)
{
	uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));
	
	if (neuronId == 0)
	{ 
		for(int i=0;i<inputSize; ++i)
		{
			if (input[i] > 99)
				input[i] = 99;

			if (input[i] < -99)
				input[i] = -99;

			// Write result to global memory
			output[i] = SIGMOID(input[i]);
		}
	}
}

// Activate sigmoide using the predefined equation constant 
// SIGMOID(x) = (1.0f / (1 + exp(-x)))
__kernel void ActivateSigmoid(
	__global double* input, __global double* output, uint inputSize, uint noneParallel)
{
	if (noneParallel)
	{
		ActivateSigmoidNoneParallel(input, output, inputSize);
	}
	else
	{ 
		uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));
	
		if (neuronId < inputSize)
		{
			// Sigmoid in c is defined only in the rang of -99..X..99 
			if (input[neuronId] > 99)
				input[neuronId] = 99;

			if (input[neuronId] < -99)
				input[neuronId] = -99;

			// Write result to global memory
			output[neuronId] = SIGMOID(input[neuronId]);
		}
	}
}

__kernel void DeactivateSigmoidNoneParallel(
	__global double* input, __global double* output, uint inputSize)
{
	uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));
	
	if (neuronId == 0)
	{ 
		for(int i=0;i<inputSize; ++i)
		{
			// Write result to global memory
			output[i] = UNSIGMOID(input[i]);
		}
	}
}

// Deactivate sigmoid using the predefined equation constant 
// UNSIGMOID(x) ((double)SIGMOID(x) * (1-SIGMOID(x)))
__kernel void DeactivateSigmoid(
	__global double* input, __global double* output, uint inputSize, uint noneParallel)
{
	if (noneParallel)
	{ 
		DeactivateSigmoidNoneParallel(input, output, inputSize);
	}
	else
	{ 
		uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));

		if (neuronId < inputSize)
		{
			// Write result to global memory
			output[neuronId] = UNSIGMOID(input[neuronId]);
		}
	}
}

__kernel void HiddenLayerFeedForwardNoneParallel(
	__global double* layerInput,
	__global double* layerWeights,
	__global double* layerOutput,
	uint inputSize, uint neuronsCount)
{
	uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));
	if (neuronId == 0)
	{
		for (int i=0; i < neuronsCount; ++i)
		{
			layerOutput[i] = 0;
			for(int j=0; j< inputSize; ++j)
			{ 
				layerOutput[i] += layerWeights[(j*neuronsCount) + i] * layerInput[j];
			}
		}
	}
}

__kernel void HiddenLayerFeedForward(
	__global double* layerInput,
	__global double* layerWeights,
	__global double* layerOutput,
	uint inputSize, uint neuronsCount, 
	uint noneParallel)
{
	if (noneParallel)
	{ 
		HiddenLayerFeedForwardNoneParallel(layerInput, layerWeights, layerOutput, inputSize, neuronsCount);
	}
	else
	{ 
		uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));

		if (neuronId < neuronsCount)
		{
			// Compute dot product  
			layerOutput[neuronId] = 0;
			for (int i = 0; i < inputSize; ++i)
			{
				layerOutput[neuronId] += layerWeights[(i*neuronsCount) + neuronId] * layerInput[i];
			}
		}
	}
}

__kernel void HiddenLayerErrorCalculationNoneParallel(
	__global double* nextLayerErrors,
	__global double* layerInputBeforeSigmoid,
	__global double* layerWeights,
	__global double* layerOutputAfterDeactivateSigmoid,
	__global double* layerOutputAfterSigmoid,
	__global double* layerErrors,
	uint layerNeuronsCount,
	uint layerNeuronWeightsCount
)
{
	for (int j=0; j < layerNeuronWeightsCount; ++j) {
		layerErrors[j] = 0;
		// Compute neuron error
		for (int i = 0; i < layerNeuronsCount; ++i)
		{
			layerErrors[j] += (layerWeights[(j * layerNeuronsCount) + i] * nextLayerErrors[i]);
		}
		layerErrors[j] = layerErrors[j] * (layerOutputAfterDeactivateSigmoid[j] * layerOutputAfterSigmoid[j]);
	}
	// Fix neuron weights
	for (int j=0; j < layerNeuronsCount; ++j)
	{
		for (int i = 0; i < layerNeuronWeightsCount; ++i)
		{
			double activatedOutput = layerOutputAfterSigmoid[i];
			layerWeights[(i*layerNeuronsCount) + j] += (LEARNING_RATE * nextLayerErrors[j] * activatedOutput);
		}
	}
}

__kernel void HiddenLayerErrorCalculation(
	__global double* nextLayerErrors,
	__global double* layerInputBeforeSigmoid,
	__global double* layerWeights,
	__global double* layerOutputAfterDeactivateSigmoid,
	__global double* layerOutputAfterSigmoid,
	__global double* layerErrors,
	uint layerNeuronsCount,
	uint layerNeuronWeightsCount,
	uint noneParallel
)
{
	if (noneParallel)
	{ 
		HiddenLayerErrorCalculationNoneParallel(nextLayerErrors, layerInputBeforeSigmoid, layerWeights, 
			layerOutputAfterDeactivateSigmoid, layerOutputAfterSigmoid, layerErrors, layerNeuronsCount, layerNeuronWeightsCount);
	}
	else
	{ 
		uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));

		if (neuronId < layerNeuronWeightsCount) {
			layerErrors[neuronId] = 0;
			// Compute neuron error
			for (int i = 0; i < layerNeuronsCount; ++i)
			{
				layerErrors[neuronId] += (layerWeights[(neuronId * layerNeuronsCount) + i] * nextLayerErrors[i]);
			}
			layerErrors[neuronId] = layerErrors[neuronId] * (layerOutputAfterDeactivateSigmoid[neuronId] * layerOutputAfterSigmoid[neuronId]);
		}
		// Fix neuron weights
		if (neuronId < layerNeuronsCount)
		{
			for (int i = 0; i < layerNeuronWeightsCount; ++i)
			{
				double activatedOutput = layerOutputAfterSigmoid[i];
				layerWeights[(i*layerNeuronsCount) + neuronId] += (LEARNING_RATE * nextLayerErrors[neuronId] * activatedOutput);
			}
		}
	}
}

__kernel void FirstHiddenLayerErrorCalculationNoneParallel(
	__global double* nextLayerErrors,
	__global double* layerInput,
	__global double* layerWeights,
	uint layerNeuronsCount,
	uint layerNeuronWeightsCount
)
{
	// Fix neuron weights
	for (int j=0; j < layerNeuronsCount; ++j)
	{
		for (int i = 0; i < layerNeuronWeightsCount; ++i)
		{
			double weight = layerWeights[(i*layerNeuronsCount) + j];
			double activatedOutput = layerInput[i];
			layerWeights[(i*layerNeuronsCount) + j] += (LEARNING_RATE * nextLayerErrors[j] * activatedOutput);
		}
	}
}


__kernel void FirstHiddenLayerErrorCalculation(
	__global double* nextLayerErrors,
	__global double* layerInput,
	__global double* layerWeights,
	uint layerNeuronsCount,
	uint layerNeuronWeightsCount,
	uint noneParallel
)
{
	if (noneParallel)
	{ 
		FirstHiddenLayerErrorCalculationNoneParallel(nextLayerErrors, layerInput, layerWeights, layerNeuronsCount, layerNeuronWeightsCount);
	}
	else
	{ 
		uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));

		// Fix neuron weights
		if (neuronId < layerNeuronsCount)
		{
			for (int i = 0; i < layerNeuronWeightsCount; ++i)
			{
				double weight = layerWeights[(i*layerNeuronsCount) + neuronId];
				double activatedOutput = layerInput[i];
				layerWeights[(i*layerNeuronsCount) + neuronId] += (LEARNING_RATE * nextLayerErrors[neuronId] * activatedOutput);
			}
		}
	}
}

__kernel void OutputLayerErrorCalculationNoneParallel(
	__global double* output,
	__global double* expectedResult,
	__global double* errors,
	uint layerNeuronsCount
)
{
	for (int i=0; i<layerNeuronsCount; ++i)
	{
		errors[i] = expectedResult[i] - output[i];
	}
}

__kernel void OutputLayerErrorCalculation(
	__global double* output,
	__global double* expectedResult,
	__global double* errors,
	uint layerNeuronsCount,
	uint noneParallel
)
{
	if (noneParallel)
	{ 
		OutputLayerErrorCalculationNoneParallel(output, expectedResult, errors, layerNeuronsCount);
	}
	else
	{ 
		uint neuronID = get_global_id(0) + (get_global_size(0) * get_global_id(1));

		if (neuronID < layerNeuronsCount) {
			errors[neuronID] = expectedResult[neuronID] - output[neuronID];
		}
	}
}

__kernel void IsSuccessful(__global double* result, __global double* label, uint size, __local bool* success)
{
	uint maxIndex = 0;
	double maxValue = result[0];

	for(uint i=1; i<size; i++)	
	{
		if (maxValue < result[i])
		{
			maxValue = result[i];
			maxIndex = i;
		}
	}
	success[0] = (label[maxIndex] == 1.0);
}

// DONE
// storage = 
// [0..a] - layer1 input vector (size = layer1 input)
// [a+1..b] - layer1 weights matrix (width = layer1 input size, height = layer1 neurons count)
// [b+1..c] - layer1 output vector (size = layer1 output)
// [c+1..d] - layer1 errors vector (size = layer1 errors)
// [d+1..e] - layer2 input vector (size = layer2 input)
// [e+1..f] - layer2 weights matrix (width = layer2 input size, height = layer2 neurons count)
// [f+1..g] - layer2 output vector (size = layer2 output)
// [g+1..h] - layer2 errors vector (size = layer2 errors)
// last layer
// [*+1..e] - last layer input vector (size = last layer input)
// [*+1..f] - last layer weights matrix (width = last layer input size, height = layer2 neurons count)
// [*+1..g] - last layer output vector (size = last layer output)
// [*+1..h] - last layer errors vector (size = last layer errors)
// [*+1..g] - last layer activated output vector (size = last layer activated output)
// [*+1..h] - last layer activated output errors vector (size = last layer activated output errors)
// ...
// settings =
// [0] - is learning,
// [1] - layers count,
// [2] - layer1 input size,
// [3] - layer1 neurons count,
// [4] - layer2 input size,
// [5] - layer2 neurons count,

__kernel void NeuralNetworkExecuter(
	__global double* storage,
	__global uint* settings,
	__global double* images,
	__global double* labels,
	__global uint* batchSize
)
{
	uint successes = 0;
	bool useBias = (0 < settings[0]);
	bool isLearning = (0 < settings[1]);
	bool noneParallel = (0 < settings[2]);
	uint layers = settings[3];
	uint inputSize = 0;
	uint neuronWeightsCount = 0;
	uint neuronsCount = 0;
	uint outputSize = 0;
	uint errorsSize = 0;
	uint nextErrorsSize = 0;
	uint prevErrorsSize = 0;
	uint offset = 0;

	uint neuronId = get_global_id(0) + (get_global_size(0) * get_global_id(1));

	if ((noneParallel) && (neuronId != 0))
	{ 
		return;
	}

	for (int j = 0; j < batchSize[0]; ++j)
	{
		offset = 0;

		__global double* feedForwardResult;
		__global uint* layerSettings = settings + 4;

		//fill first layer input in parallel
		//if (neuronId < layerSettings[0])
		//{
		//	storage[neuronId] = images[(j*layerSettings[0]) + neuronId];
		//}

		if (neuronId == 0)
		{
			for (int k=0; k < layerSettings[0]; ++k)
			{ 
				storage[k] = images[(j*layerSettings[0]) + k];
			}
		}

		barrier(CLK_GLOBAL_MEM_FENCE);
		//FEED FORWARD
		for (uint i = 0; i < layers; ++i)
		{
			inputSize = layerSettings[0];
			neuronsCount = layerSettings[1];
			outputSize = layerSettings[1];
			errorsSize = inputSize;// errors per wheight for each neuron

			__global double* layerInput = &(storage[offset]);
			__global double* layerWeights = &(storage[offset + inputSize]);
			__global double* layerOutput = &(storage[offset + inputSize + (inputSize * neuronsCount)]);
			__global double* layerErrors = &(storage[offset + inputSize + (inputSize * neuronsCount) + outputSize]);
			__global double* layerDeactivatedOutput = &(storage[offset + inputSize + (inputSize * neuronsCount) + outputSize + errorsSize]);
			__global double* layerActivatedOutput = &(storage[offset + inputSize + (inputSize * neuronsCount) + outputSize + errorsSize + outputSize]);

			if (useBias)
			{
				if (neuronId == 0)
				{
					layerInput[neuronId] = 1;
				}
			}
			barrier(CLK_GLOBAL_MEM_FENCE);
			/*if ((neuronId==0) && (isLearning) && (i > 0))
			{
				printf("\n\n\n\n");
				printf("layerInput\n");
				for (int k=0; k<inputSize; ++k)
				{
					printf("[%.15f]",layerInput[k]);
				}
				printf("\n\n\n\n");
				PrintMatrix("layerWeights", layerWeights, neuronsCount, inputSize);
				printf("\n\n\n\n");
			}*/
			if (neuronId < neuronsCount)
			{
				HiddenLayerFeedForward(layerInput, layerWeights, layerOutput, inputSize, neuronsCount, noneParallel);
				ActivateSigmoid(layerOutput, layerActivatedOutput, outputSize, noneParallel);
				DeactivateSigmoid(layerOutput, layerDeactivatedOutput, outputSize, noneParallel);
			}
			barrier(CLK_GLOBAL_MEM_FENCE);

			if ((neuronId==0) && (isLearning))
			{
				//PrintMatrix("layerOutput", layerOutput, outputSize, 1);
				//printf("\n\n\n\n");
			}
			// no need to advance the pointer after the last layer
			// this should be the starting point for the back propagation
			if (i < layers - 1)
			{
				offset = offset + inputSize + (inputSize * neuronsCount) + outputSize + errorsSize + outputSize;
				layerSettings = layerSettings + 2;
			}
			barrier(CLK_GLOBAL_MEM_FENCE);
			feedForwardResult = layerActivatedOutput;

			if ((neuronId==0) && (isLearning) && (i==2))
			{ 
				//PrintMatrix("layerActivatedOutput", layerActivatedOutput, outputSize, 1);
				//printf("\n\n\n\n");
			}
		}

		if (!isLearning)
		{
			if (neuronId == 0)
			{
				if (j == batchSize[0] - 1)
				{
					PrintMatrix("Output", feedForwardResult, outputSize, 1);
				}

				__local bool success[1];
				IsSuccessful(feedForwardResult, &labels[j*outputSize], outputSize, success);
				if (success[0])
				{
					successes++;
				}
			}
		}
		else
		{
			inputSize = layerSettings[0];
			neuronWeightsCount = layerSettings[0];
			neuronsCount = layerSettings[1];
			outputSize = layerSettings[1];
			errorsSize = neuronWeightsCount;
			nextErrorsSize = outputSize;
			prevErrorsSize = (layerSettings - 2)[0];

			//BACK PROPAGATION
			__global double* layerLabel = &labels[j * outputSize];
			__global double* nextLayerErrors;
			//go layer by layer to the start
			for (int i = 0; i < layers; i++)
			{
				// the input size is the actually the output size of the previouse layer
				// and the previous layer ends with Size(layerActivatedOutput + layerErrors + layerOutput) = 3*inputSize 
				__global double* layerInputBeforeSigmoid = &(storage[offset - (inputSize + prevErrorsSize + inputSize)]);
				__global double* layerInput = &(storage[offset]);
				__global double* layerWeights = &(storage[offset + inputSize]);
				__global double* layerOutput = &(storage[offset + inputSize + (inputSize*neuronsCount)]);
				__global double* layerErrors = &(storage[offset + inputSize + (inputSize*neuronsCount) + outputSize]);
				__global double* layerDeactivatedOutput = &(storage[offset + inputSize + (inputSize*neuronsCount) + outputSize + errorsSize]);
				__global double* layerActivatedOutput = &(storage[offset + inputSize + (inputSize*neuronsCount) + outputSize + errorsSize + outputSize]);
				__global double* prevLayerDeactivatedOutput = &(storage[offset - inputSize]); //layerErrors + errorsSize;
				__global double* prevLayerActivatedOutput = &(storage[offset]);// layerDeactivatedOutput + outputSize;
				
				barrier(CLK_GLOBAL_MEM_FENCE);
				// do back propagatio
				if (i == 0) // output layer
				{
					// Last layerholds a special place for the nextLayerErrors 
					// because there is no next layer then the nextLayerErrors are calculated 
					// by subtracting the layer activated output from the label
					nextLayerErrors = &layerActivatedOutput[outputSize];
					OutputLayerErrorCalculation(layerActivatedOutput, layerLabel, nextLayerErrors, outputSize, noneParallel);
					barrier(CLK_GLOBAL_MEM_FENCE);
					HiddenLayerErrorCalculation(nextLayerErrors, layerInputBeforeSigmoid, layerWeights, prevLayerDeactivatedOutput, prevLayerActivatedOutput, layerErrors, neuronsCount, inputSize, noneParallel);
				}
				else if (i == (layers - 1)) // input layer
				{
					FirstHiddenLayerErrorCalculation(nextLayerErrors, layerInput, layerWeights, neuronsCount, inputSize, noneParallel);
				}
				else // hidden layer
				{
					HiddenLayerErrorCalculation(nextLayerErrors, layerInputBeforeSigmoid, layerWeights, prevLayerDeactivatedOutput, prevLayerActivatedOutput, layerErrors, neuronsCount, inputSize, noneParallel); 
				}
				barrier(CLK_GLOBAL_MEM_FENCE);
				if ((neuronId==0) && (isLearning) && (i == 2))
				{
					//printf("\n\n\n\n");
					//printf("layerInput\n");
					//for (int k=0; k<inputSize; ++k)
					//{
						//printf("[%.15f]",layerInput[k]);
					//}
					//printf("\n\n\n\n");
					//PrintMatrix("layerWeights", layerWeights, neuronsCount, inputSize);
					//printf("\n\n\n\n");
				}

				nextLayerErrors = layerErrors;
				if (i < layers - 1)
				{
					// move one layer backwards
					layerSettings = layerSettings - 2;

					inputSize = layerSettings[0];
					neuronWeightsCount = layerSettings[0];
					neuronsCount = layerSettings[1];
					outputSize = layerSettings[1];
					errorsSize = neuronWeightsCount;
					nextErrorsSize = outputSize;
					if (i < layers - 2)
					{
						prevErrorsSize = (layerSettings - 2)[0];// looking on the latest layer
					}
					offset = offset - (inputSize + (inputSize * neuronsCount) + outputSize + errorsSize + outputSize);
				}
				barrier(CLK_GLOBAL_MEM_FENCE);
			}
		}
	}

	if (!isLearning)
	{
		if (neuronId == 0)
		{
			printf("batch count: %d ",batchSize[0]);
			printf("success: %d ",successes);
			printf("error: %f% \n", (1 - ((double)successes / (double)batchSize[0])));
		}
	}
	// ALL WORK ITEMS MUST PASS HERE
	barrier(CLK_GLOBAL_MEM_FENCE);
}
