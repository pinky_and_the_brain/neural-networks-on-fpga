#pragma once

typedef unsigned char uchar;

struct GeneralDataSet
{
	bool useBias = false;

	int labelSize = 3;
	int recordSize = 13;
	int recordsCount = 178;

	double** records;
	double** labels;

	int trainRecordsCount = 178;
	double** trainRecords;
	double** trainLabels;

	int level1MatrixWidth = 13;
	int level1MatrixHeight = 20;
	double** level1Matrix;

	int level2MatrixWidth = 20;
	int level2MatrixHeight = 10;
	double** level2Matrix;

	int level3MatrixWidth = 10;
	int level3MatrixHeight = 3;
	double** level3Matrix;
};

GeneralDataSet GetXORDataSet();
GeneralDataSet GetWinesDataSet();
double** ReadRecords(char* fullPath, int& recordsCount, int& recordSize);
double** ReadRecords(char* fullPath, int& recordsCount, int& recordSize, bool skipFirstColumn, bool useBias);
double** ReadLabels(char* fullPath, int& recordsCount, int& labelSize);
double** ReadMatrix(char* fullPath, int& matrixWidth, int& matrixHeight);

GeneralDataSet GetMnstDataSet();
double** ReadMnistRecords(char* fullPath, int& recordsCount, int& recordSize, bool isBias);
double** ReadMnistLabels(char* fullPath, int& recordsCount, int& labelSize);

