#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <time.h>
#include <memory.h>
#include <windows.h>
#include "CL\cl.h"
#include "CL\cl_ext.h"
#include "utils.h"
#include <assert.h>


//we want to use POSIX functions
#pragma warning( push )
#pragma warning( disable : 4996 )

/// <summary>
/// Print log notification to the default output. 
/// </summary>
/// <remarks>Same usage as with printf.</remarks>
/// <param name="str">Log description</param>
/// <param name="...">Arguments for string format as in printf</param>
void LogInfo(const char* str, ...)
{
    if (str)
    {
        va_list args;
        va_start(args, str);

        vfprintf(stdout, str, args);

        va_end(args);
    }
}

/// <summary>
/// Print error notification to the default output. 
/// </summary>
/// <remarks>Same usage as with printf.</remarks>
/// <param name="str">Error description</param>
/// <param name="...">Arguments for string format as in printf</param>
void LogError(const char* str, ...)
{
    if (str)
    {
        va_list args;
        va_start(args, str);

        vfprintf(stderr, str, args);

        va_end(args);
    }
}

/// <summary>
/// Generate an integer random value ranged between from and to values
/// </summary>
/// <param name="from">Lowest limit value</param>
/// <param name="to">Highes limit value</param>
/// <returns>Randome value</returns>
int GetRandIntFromRange(int from, int to)
{
	static bool first = true;
	if (first)
	{
		srand(time(NULL)); //seeding for the first time only!
		first = false;
	}
	return from + rand() % ((to + 1) - from);
}
#pragma warning( pop )