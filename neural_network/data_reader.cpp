#include "data_reader.h"
#include <fstream> 
#include <stdexcept>
#include <iostream>
#include <string>
#include <sstream> 

GeneralDataSet GetXORDataSet()
{
	GeneralDataSet dataset;

	dataset.recordsCount = 4;
	dataset.trainRecordsCount = 4;
	dataset.labelSize = 1;
	dataset.recordSize = 6;

	dataset.trainRecords = ReadRecords("./DB/XOR/xor.data", dataset.recordsCount, dataset.recordSize, true, dataset.useBias);
	dataset.trainLabels = ReadLabels("./DB/XOR/xor.data", dataset.recordsCount, dataset.labelSize);

	dataset.records = ReadRecords("./DB/XOR/xor.data", dataset.trainRecordsCount, dataset.recordSize, true, dataset.useBias);
	dataset.labels = ReadLabels("./DB/XOR/xor.data", dataset.trainRecordsCount, dataset.labelSize);

	dataset.level1MatrixWidth = 4;
	dataset.level1MatrixHeight = 6;
	dataset.level1Matrix = ReadMatrix("./DB/XOR/weight1.data", dataset.level1MatrixWidth, dataset.level1MatrixHeight);

	dataset.level2MatrixWidth = 2;
	dataset.level2MatrixHeight = 4;
	dataset.level2Matrix = ReadMatrix("./DB/XOR/weight2.data", dataset.level2MatrixWidth, dataset.level2MatrixHeight);

	dataset.level3MatrixWidth = 1;
	dataset.level3MatrixHeight = 2;
	dataset.level3Matrix = ReadMatrix("./DB/XOR/weight3.data", dataset.level3MatrixWidth, dataset.level3MatrixHeight);

	return dataset;
}

GeneralDataSet GetWinesDataSet()
{
	GeneralDataSet dataset;

	dataset.recordsCount = 178;
	dataset.trainRecordsCount = 178;
	dataset.labelSize = 3;
	dataset.recordSize = 13;
	dataset.useBias = true;

	if (dataset.useBias)
		dataset.recordSize++;

	dataset.trainRecords = ReadRecords("./DB/Wine/wines-normalized.data", dataset.trainRecordsCount, dataset.recordSize, true, dataset.useBias);
	dataset.trainLabels = ReadLabels("./DB/Wine/wines-normalized.data", dataset.trainRecordsCount, dataset.labelSize);

	dataset.records = ReadRecords("./DB/Wine/wines-normalized.data", dataset.recordsCount, dataset.recordSize, true, dataset.useBias);
	dataset.labels = ReadLabels("./DB/Wine/wines-normalized.data", dataset.recordsCount, dataset.labelSize);

	dataset.level1MatrixWidth = 20;
	dataset.level1MatrixHeight = dataset.recordSize;
	dataset.level1Matrix = ReadMatrix("./DB/Wine/weight1.data", dataset.level1MatrixWidth, dataset.level1MatrixHeight);

	dataset.level2MatrixWidth = 10;
	dataset.level2MatrixHeight = 20;
	dataset.level2Matrix = ReadMatrix("./DB/Wine/weight2.data", dataset.level2MatrixWidth, dataset.level2MatrixHeight);

	dataset.level3MatrixWidth = 3;
	dataset.level3MatrixHeight = 10;
	dataset.level3Matrix = ReadMatrix("./DB/Wine/weight3.data", dataset.level3MatrixWidth, dataset.level3MatrixHeight);
	
	return dataset;
}

double** ReadRecords(char* fullPath, int& recordsCount, int& recordSize)
{
	return ReadRecords(fullPath, recordsCount, recordSize, false, false);
}

double** ReadRecords(char* fullPath, int& recordsCount, int& recordSize, bool skipFirstColumn, bool useBias)
{
	double** records;
	std::string line;
	std::fstream file(fullPath, std::ios::in);

	records = new double*[recordsCount];
	for (int i = 0; i < recordsCount; ++i)
	{
		records[i] = new double[recordSize];
	}

	int i = 0;
	while (std::getline(file, line) && i < recordsCount)
	{
		std::stringstream linestream(line);
		std::string value;

		int j = 0;
		if (useBias)
		{
			records[i][j] = 1;
			j++;
		}
		while (std::getline(linestream, value, ','))
		{
			if ((!skipFirstColumn) || ((useBias) && (j > 1)) || ((!useBias) && (j > 0)))
			{
				if (skipFirstColumn)
				{
					records[i][j-1] = std::stod(value);
				}
				else
				{
					records[i][j] = std::stod(value);
				}
			}
			j++;
		}
		i++;
	}
	file.close();
	return records;
}

double** ReadLabels(char* fullPath, int& recordsCount, int& labelSize)
{
	double** labels;

	std::string line;
	std::fstream file(fullPath, std::ios::in);

	labels = new double*[recordsCount];
	for (int i = 0; i < recordsCount; ++i)
	{
		labels[i] = new double[labelSize];
	}

	int i = 0;
	while (std::getline(file, line) && i < recordsCount)
	{
		std::stringstream linestream(line);
		std::string value;
		std::getline(linestream, value, ',');
		int label = std::stoi(value);
		for (int j = 0; j < labelSize; ++j)
		{
			if (j == label - 1)
			{
				labels[i][j] = 1;
			}
			else
			{
				labels[i][j] = 0;
			}
		}
		i++;
	}
	file.close();
	return labels;
}

double** ReadMatrix(char* fullPath, int& matrixWidth, int& matrixHeight)
{
	return ReadRecords(fullPath, matrixHeight, matrixWidth);
}

GeneralDataSet GetMnstDataSet()
{
	GeneralDataSet dataset;

	dataset.recordsCount = 10;
	dataset.trainRecordsCount = 10;
	dataset.recordSize = 784;
	dataset.labelSize = 10;
	dataset.useBias = true;
							  
	if (dataset.useBias)
		dataset.recordSize++;
	
	dataset.trainRecords = ReadMnistRecords("./DB/Mnist/MnstTrain60k.data", dataset.trainRecordsCount, dataset.recordSize, dataset.useBias);
	dataset.trainLabels = ReadLabels("./DB/Mnist/MnstTrain60kLabels.data", dataset.trainRecordsCount, dataset.labelSize);

	//dataset.records = ReadMnistRecords("./DB/Mnist/MnstTrain60k.data", dataset.recordsCount, dataset.recordSize, dataset.useBias);
	//dataset.labels = ReadLabels("./DB/Mnist/MnstTrain60kLabels.data", dataset.recordsCount, dataset.labelSize);

	dataset.records = ReadMnistRecords("./DB/Mnist/MnstTest10k.data", dataset.recordsCount, dataset.recordSize, dataset.useBias);
	dataset.labels = ReadLabels("./DB/Mnist/MnstTest10kLabels.data", dataset.recordsCount, dataset.labelSize);

	dataset.level1MatrixWidth = 50;
	dataset.level1MatrixHeight = dataset.recordSize;
	dataset.level1Matrix = ReadMatrix("./DB/Mnist/weight1-bias.data", dataset.level1MatrixWidth, dataset.level1MatrixHeight);

	dataset.level2MatrixWidth = 20;
	dataset.level2MatrixHeight = 50;
	dataset.level2Matrix = ReadMatrix("./DB/Mnist/weight2.data", dataset.level2MatrixWidth, dataset.level2MatrixHeight);

	dataset.level3MatrixWidth = 10;
	dataset.level3MatrixHeight = 20;
	dataset.level3Matrix = ReadMatrix("./DB/Mnist/weight3.data", dataset.level3MatrixWidth, dataset.level3MatrixHeight);

	return dataset;
}

double** ReadMnistRecords(char* fullPath, int& recordsCount, int& recordSize, bool useBias)
{
	//auto reverseInt = [](int i) {
	//	unsigned char c1, c2, c3, c4;
	//	c1 = i & 255, c2 = (i >> 8) & 255, c3 = (i >> 16) & 255, c4 = (i >> 24) & 255;
	//	return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
	//};

	//int newRecordSize = recordSize;
	//if (useBias)
	//	newRecordSize++;

	//std::ifstream file(fullPath, std::ios::binary);

	//if (file.is_open()) {
	//	int magic_number = 0, n_rows = 0, n_cols = 0;

	//	file.read((char *)&magic_number, sizeof(magic_number));
	//	magic_number = reverseInt(magic_number);

	//	if (magic_number != 2051) throw std::runtime_error("Invalid MNIST image file!");

	//	file.read((char *)&recordsCount, sizeof(recordsCount)), recordsCount = reverseInt(recordsCount);
	//	file.read((char *)&n_rows, sizeof(n_rows)), n_rows = reverseInt(n_rows);
	//	file.read((char *)&n_cols, sizeof(n_cols)), n_cols = reverseInt(n_cols);

	//	//image_size = n_rows * n_cols;

	//	uchar** _dataset = new uchar*[recordsCount];
	//	for (int i = 0; i < recordsCount; i++) {
	//		_dataset[i] = new uchar[recordSize];
	//		file.read((char *)_dataset[i], recordSize);
	//	}

		double** doubledataSet = ReadRecords(fullPath, recordsCount, recordSize, false, useBias);
		for (int i = 0; i < recordsCount; i++) {
			int j = 0;
			if (useBias)
			{
				j++;
			}
			for (; j < recordSize; j++) {

				doubledataSet[i][j] = ((double)doubledataSet[i][j] / (double)255.0f);
			}
		}

		return doubledataSet;
	//}
	//else {
	//	throw std::runtime_error("Cannot open file `" + *fullPath);
	//}
}

double** ReadMnistLabels(char* fullPath, int& recordsCount, int& labelSize)
{
	auto reverseInt = [](int i) {
		unsigned char c1, c2, c3, c4;
		c1 = i & 255, c2 = (i >> 8) & 255, c3 = (i >> 16) & 255, c4 = (i >> 24) & 255;
		return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
	};

	std::ifstream file(fullPath, std::ios::binary);

	if (file.is_open()) {
		int magic_number = 0;
		file.read((char *)&magic_number, sizeof(magic_number));
		magic_number = reverseInt(magic_number);

		if (magic_number != 2049) throw std::runtime_error("Invalid MNIST label file!");

		file.read((char *)&recordsCount, sizeof(recordsCount)), recordsCount = reverseInt(recordsCount);

		uchar* _dataset = new uchar[recordsCount];
		for (int i = 0; i < recordsCount; i++) {
			file.read((char*)&_dataset[i], 1);
		}

		double** labeldataSet = new double*[recordsCount];
		for (int i = 0; i < recordsCount; i++) {

			labeldataSet[i] = new double[10];
			uchar currentLabel = _dataset[i];
			for (int j = 0; j < 10; j++) {
				labeldataSet[i][j] = 0;
			}
			labeldataSet[i][(int)currentLabel] = 1;
		}
		return labeldataSet;
	}
	else {
		throw std::runtime_error("Unable to open file " + *fullPath);
	}
}