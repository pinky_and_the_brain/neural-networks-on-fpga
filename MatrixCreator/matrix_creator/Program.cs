﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrix_creator
{
    class Program
    {
        static string outputFileName = "";
        static int matrixHeight = 0;
        static int matrixWidth = 0;
        static void Main(string[] args)
        {
            AnalizeArgument(args[0], args[1]);
            AnalizeArgument(args[2], args[3]);
            AnalizeArgument(args[4], args[5]);
            CreateMatrix();
        }

        private static void CreateMatrix()
        {
            Random rand = new Random();
            using (System.IO.StreamWriter writer = System.IO.File.CreateText(outputFileName))
            {
                for (int i = 0; i < matrixHeight; ++i)
                {
                    for (int j = 0; j < matrixWidth; ++j)
                    {
                        double number = rand.NextDouble();
                        writer.Write(number.ToString("0.######").PadRight(8,'0'));
                        if (j< matrixWidth-1)
                        {
                            writer.Write(',');
                        }
                    }
                    if (i < matrixHeight - 1)
                    {
                        writer.Write(writer.NewLine);
                    }
                }
                writer.Flush();
                writer.Close();
            }
        }

        public static float Generate(Random prng)
        {
            var sign = prng.Next(2);
            var exponent = prng.Next((1 << 8) - 1); // do not generate 0xFF (infinities and NaN)
            var mantissa = prng.Next(1 << 23);

            var bits = (0 << 31) + (0 << 23) + mantissa;
            return IntBitsToFloat(bits);
        }

        private static float IntBitsToFloat(int bits)
        {
            unsafe
            {
                return *(float*)&bits;
            }
        }

        private static void AnalizeArgument(string v1, string v2)
        {
            if (v1 == "-o")
            {
                outputFileName = v2;
            }
            else if (v1 == "-h")
            {
                matrixHeight = int.Parse(v2);
            }
            else if (v1 == "-w")
            {
                matrixWidth = int.Parse(v2);
            }
        }
    }
}
